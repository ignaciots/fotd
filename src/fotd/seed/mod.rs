use chrono::{DateTime, Local, Timelike};

/// Generates a `u128` seed for the given [`DateTime<Local>`]
/// The seed will be dependant of the datetime day.
///
/// That is, for the same day the returned seed will always be the same, independently of the hours, minutes and seconds.
pub fn generate_seed(current_datetime: DateTime<Local>) -> u128 {
    current_datetime
        .with_hour(0)
        .unwrap()
        .with_minute(0)
        .unwrap()
        .with_second(0)
        .unwrap()
        .with_nanosecond(0)
        .unwrap()
        .timestamp_millis() as u128
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::TimeZone;

    #[test]
    fn test_generate_seed_today() {
        let today_local = Local::now();
        assert_eq!(generate_seed(today_local), generate_seed(today_local))
    }

    #[test]
    fn test_generate_seed_fixed() {
        let fixed_day = Local.with_ymd_and_hms(2020, 5, 1, 4, 12, 33);
        let fixed_day_different_hour = Local.with_ymd_and_hms(2020, 5, 1, 6, 3, 12);
        let fixed_day_different = Local.with_ymd_and_hms(2020, 3, 22, 15, 7, 47);

        assert_eq!(
            generate_seed(fixed_day.single().unwrap()),
            generate_seed(fixed_day_different_hour.single().unwrap())
        );

        assert_ne!(fixed_day, fixed_day_different)
    }
}
