use super::FotdError;
use std::error::Error;
use std::fs::DirEntry;
use std::path::{Path, PathBuf};

///  Gets a file path from the given directory path.
/// From the same directory structure and the same `seed`, the returned file path will always be the same.
///
/// Returns `PathBuf` if a file path has been selected correctly, or `Error` if any error occured while reading the directory structure.
pub fn get_file(file_path: &Path, seed: usize) -> Result<PathBuf, Box<dyn Error>> {
    let files: Vec<DirEntry> = file_path
        .read_dir()?
        // Map io Error to FotdError, checking metadata access
        .map(|entry| match entry {
            Ok(dir) => match dir.metadata() {
                Ok(_) => Ok(dir),
                Err(e) => Err(Box::new(FotdError::new(format!(
                    "{}: {}",
                    e,
                    get_entry_name(&dir)
                )))),
            },
            Err(e) => Err(Box::new(FotdError::new(format!("{}", e)))),
        })
        .collect::<Result<Vec<DirEntry>, Box<FotdError>>>()?;

    // Filter by file
    let files = files
        .into_iter()
        .filter(|entry| {
            entry
                .metadata()
                .unwrap_or_else(|_| {
                    panic!("Could not read metadata of file {}", get_entry_name(entry))
                })
                .is_file()
        })
        .collect::<Vec<DirEntry>>();

    if files.is_empty() {
        return Err(Box::new(FotdError {
            message: String::from("Target directory does not contain valid files"),
        }));
    }

    let selected_file = &files[seed % files.len()];
    Ok(selected_file.path())
}

fn get_entry_name(entry: &DirEntry) -> String {
    entry.file_name().into_string().expect(
        "File's metafile could not be read and its name does not contain valid Unicode data.",
    )
}
