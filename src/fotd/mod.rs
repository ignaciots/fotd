//! Module that contains the main *fotd* functionality.
//!
//! *fotd* (file of the day) chooses a random daily file (or file) based on a number of configurations, such as target directory or seed.
use chrono::Local;
use std::error::Error;
use std::fmt;
use std::path::Path;

mod file;
mod seed;

/// Struct that contains generic fotd application errors
#[derive(Debug)]
#[doc(hidden)]
pub struct FotdError {
    pub message: String,
}

impl FotdError {
    pub fn new(message: String) -> FotdError {
        FotdError { message }
    }
}

impl fmt::Display for FotdError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "fotd: error: {}", self.message)
    }
}

impl Error for FotdError {}

/// Struct with the necessary options that must be passed to [fotd]
#[derive(Debug)]
pub struct Options {
    /// Target directory of the application.
    pub dir: String,
    /// Optional custom seed provided to the application.
    pub seed: Option<u128>,
}

impl Options {
    /// Creates a new [Options] struct using the default values.
    ///
    /// - The `dir` will be `/usr/local/fotd`.
    /// - The `seed` will be `None`
    ///
    /// # Examples
    /// ```
    /// use fotd::fotd::Options;
    ///
    /// let my_options = Options::new();
    /// assert_eq!(my_options.dir, "/usr/local/fotd");
    /// assert_eq!(my_options.seed, None);
    /// ```
    pub fn new() -> Options {
        Options {
            dir: String::from("/usr/local/fotd"),
            seed: None,
        }
    }
}

impl Default for Options {
    fn default() -> Self {
        Self::new()
    }
}

/// Executes the fotd application using the given [Options]
///
/// If [Options] contains a `seed`, this seed will be used to calculate the file path. Otherwise, a seed will be retrieved based on the current **system local date** (time independent).
///
/// Using this `seed`, files will be read from the target `dir`, and a random valid file will be chosen.
///
/// Finally, the `msg` will be printed if `show_message` is set to true and, after that, the chosen file path will be printed.
///
/// Returns the chosen path as a [String], or an [Error] if errors occured while reading the target directory and its contents.
pub fn fotd(options: &Options) -> Result<String, Box<dyn Error>> {
    let current_seed: u128;
    if let Some(seed) = &options.seed {
        current_seed = *seed;
    } else {
        current_seed = seed::generate_seed(Local::now());
    }

    let file_path = file::get_file(Path::new(&options.dir), current_seed.try_into().unwrap());

    let string_path = file_path?;
    match string_path.to_str() {
        Some(path) => {
            println!("{}", path);
            Ok(path.to_string())
        }
        None => Err(Box::new(FotdError {
            message: String::from(
                "Could not convert path to string. Path contains non UTF-8 characters",
            ),
        })),
    }
}
