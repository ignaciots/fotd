use std::process;

fn main() {
    if let Err(e) =fotd::run() {
        eprintln!("fotd error: {}", e);
        process::exit(1);
    }
}
