//! *fotd* (file of the day) chooses a random daily file (or file) based on a number of parameter configurations, such as target directory or seed, and prints the chosen file path.
use argparse::{ArgumentParser, Print, Store, StoreOption};
use std::env;
use std::error::Error;

pub mod fotd;

/// Runs the fotd program
pub fn run() -> Result<(), Box<dyn Error>> {
    let mut options = fotd::Options::new();

    {
        let mut parser = ArgumentParser::new();
        parser.set_description("Retrieve the file of the day");
        parser.add_option(
            &["-v", "--version"],
            Print(String::from("fotd ") + env!("CARGO_PKG_VERSION")),
            "Show version",
        );
        parser
            .refer(&mut options.dir)
            .add_option(&["-d", "--dir"], Store, "Location of the files of the day.")
            .required();
        parser.refer(&mut options.seed).add_option(
            &["-s", "--seed"],
            StoreOption,
            "Seed used by the PRNG to retrieve the file of the day.",
        );

        parser.parse_args_or_exit();
    }

    match fotd::fotd(&options) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}
