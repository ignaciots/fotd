# fotd

File of the day: Utilidad para obtener un archivo aleatorio en un directorio; este archivo será consistente a lo largo del día.

## Uso

```
fotd -d midirectorio
```

Esto devolverá la ruta de un archivo aleatorio del directorio, el cual siempre será el mismo para un día dado.


